# Ubuntu Server Setup log

## Information

* User: Peng Linging

* IP: 161.122.102.32

* Subnet Mask: 255.255.255.0

* Default Gateway: 161.122.102.1

* Preferred DNS Server: 161.122.37.3

* Second DNS Server: 134.75.30.1


## Verify iso:

* [refer to link for instructions](https://askubuntu.com/questions/867339/how-to-verify-iso-from-https-www-ubuntu-com-official-site-from-os-x-mac)

* *sha256sum -b ubuntu-16.04.4-server-amd64.iso*


## Installing Ubuntu Server 16.04.4:

### to “quiet splash”:

* [forum guide link](https://devtalk.nvidia.com/default/topic/1022396/linux/pcie-bus-error-severity-corrected-after-booting-into-ubuntu-16-04-2-lts-/)

* Boot into Ubuntu recovery mode (get to grub code)

* *GRUB_CMDLINE_LINUX_DEFAULT="quiet splash pci=nomsi"*

* and save the file with: *Ctrl+o*, then Enter

* Close nano with: *Ctrl+x*

* next update grub: *sudo update-grub*

* Follow by reboot your system: *reboot*

* If still getting continuous error messages:

* *GRUB_CMDLINE_LINUX_DEFAULT="quiet splash pci=nommconf"*

  > *OR* 

  > *GRUB_CMDLINE_LINUX_DEFAULT=“quiet splash pci=noacpi”*


## Installing Ubuntu Desktop 16.04.4:

* create login (ie chrisp)

* Establish Ethernet connection (manually input IP/gateway/DNS etc.)

### Install/update graphics drivers

* To install latest graphics drivers add PPA :

* *sudo add-apt-repository ppa:graphics-drivers/ppa*
  *sudo apt update*

* Access “Software & Updates”

* Open Additional Drivers

* Switch to “NVIDIA binary driver-version 390.25 from nvidia-390 (open source)”

* Apply changes

### Create SSH connection

* (note: start ssh keygen on host, machine that will have access to remote)

* [refer to link for guide](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2)

### Setup Basic Firewall

* [refer to link for guide](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-16-04)

### Show network adapters (Ethernet) and IP address

* $ *ifconfig*

### Setup NGINX Server

* [link to guide](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-16-04)

### Install MySQL to Manage Site Data & PHP for Dynamic Content (LEMP stack)

* [link to guide](https://www.digitalocean.com/community/tutorials/how-to-install-linux-nginx-mysql-php-lemp-stack-in-ubuntu-16-04)

### Set up NGINX Server Blocks (Virtual Hosts)

* [link to guide](https://www.digitalocean.com/community/tutorials/how-to-set-up-nginx-server-blocks-virtual-hosts-on-ubuntu-16-04)

## Flask framework introduction with Python (homepage, static webpages, routes, links)

* [link to tutorial](https://code.tutsplus.com/tutorials/an-introduction-to-pythons-flask-framework--net-28822)

* (to activate vitualenv - go to test directory)
* (*note:* use 'sudo' to create virtualenv if no ~/bin/activate)
* $ *. bin/activate*

* (if *sudo* was used, change permission of directory)
* $ *sudo chown -R $USER:$USER (directory_name)*

* (enable localhost:5000 access for viewing)
* $ *python routes.py*

### Open port 5000 for external viewing access

* add line to *routes.py*
* *if __name__ == '__main__':* (below this line)
*   *app.run(host='0.0.0.0', debug = False)*

### Add images to Static page

* *<img src="/static/img/Eum_center_cover.png">*

### Add "sign-up" and "sign in" links to layout.html

*         <div class="menu">
*            <nav>
*                <ul class="nav nav-pills pull-right">
*                    <li role="presentation"><a href="#">Sign In</a>
*                    </li>
*                    <li role="presentation"><a href="#">Sign Up</a>
*                    </li>
*                </ul>
*            </nav>
*        </div>

## Formatting HTML (homepage, etc.)

* [link to guide](https://www.w3schools.com/html/default.asp)

### Install Sublime-text

* [link to guide](http://tipsonubuntu.com/2017/05/30/install-sublime-text-3-ubuntu-16-04-official-way/)

### Create MySQL databse

* (install in virtualenv)
* [link to guide for initial setup with Flask](http://flask-mysqldb.readthedocs.io/en/latest/)

* [ link to setup guide](https://www.tutorialspoint.com/mysql/mysql-installation.htm)

#### Troubleshooting - if Command "python setup.py egg_info" failed with error code 1 in /tmp/pip-build-_bcpwf/mysqlclient/

* [link to forum](https://github.com/facebook/prophet/issues/140)

#### Troubleshooting MySQL

* (start MySQL with login)
* $ *mysql -u root -p*
* (stop MySQL)
* [$ *QUIT*]

* (to start MySQL)
* $ */etc/init.d/mysql start*
* *stop* (to stop)

#### MySQL 5.7 Tutorial

* [link to guide](https://dev.mysql.com/doc/refman/5.7/en/tutorial.html)

#### Install SQLAlchemy, mysqldb, myseql-server etc.

* $ *sudo pip install SQLAlchemy*
* $ *sudo pip install --upgrade pip*
* $ *sudo apt-get install python-psycopg2*
* $ *sudo apt-get install python-mysqldb*
* $ *sudo apt-get install mysql-server*

#### Create database and connect to Flask with SQLAlchemy

* [link to tutorial](https://www.blog.pythonlibrary.org/2017/12/12/flask-101-adding-a-database/)

* (to create virtualenv)
* $ *virtualenv (directory name)*
* (use sudo if necessary, refer to "Flask framework intro..." section)
* (while in active bin...)
* $ *pip install flask*
* $ *pip install flask-sqlalchemy*
* $ *pip install Flask-WTF*
* $ *pip install flask_table*
* $ *pip install psycopg2-binary*
* $ *pip install flask_security*

* (combined)
* $ *pip install flask flask-sqlalchemy flask-wtf flask_table psycopg2-binary flask_security*

### Create database with schema

* [link to site](https://app.quickdatabasediagrams.com/#/schema/88uw66H310-3Tfi6PaK2_Q)

#### PostgreSQL tutorial

* [link to tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-16-04)

* (to run through terminal)
* $ *sudo -i -u postgres*
* $ *psql*

## If "boot" memory is full... remove old kernels

* $ *sudo apt install byobu*
* $ *sudo purge-old-kernels*

* if not enough memory to install byobu...
* (view old kernels, not current one)
* $ *sudo dpkg --list 'linux-image*'|awk '{ if ($1=="ii") print $2}'|grep -v `uname -r`*

* (remove old verions, brackets represent different versions)
* $ sudo rm -rf /boot/*-4.13.0-{36,37,38}-*
* $ *$ sudo apt-get -f install*
* $ *sudo apt-get autoremove*

* [link](https://gist.github.com/ipbastola/2760cfc28be62a5ee10036851c654600)
* $ *sudo dpkg --list 'linux-image*'|awk '{ if ($1=="ii") print $2}'|grep -vuname -r| while read -r line; do sudo apt-get -y purge $line;done;sudo apt-get autoremove; sudo update-grub*

* [manually remove kernels](https://ubuntuforums.org/showthread.php?t=2240240&page=2&s=c56525821192c8d568a74b62608469c8)

* [troubleshoot](https://itsfoss.com/fix-ubuntu-install-error/)

### Flask Postgre

* (run in terminal in active env)
* $ *python*
* $ *from app import db*
* $ *db.create_all()*

## Install Python3 virtual env package

* $ *sudo apt-get install python3-venv*

### Create python3 virtenv

* $ *python3 -m venv (envname)*

* (activation is the same *. bin/activate*)
* $ *pip install wheel*

* (upgrade pip)
* $ *pip install --upgrade pip*

* (procede to install pip packages from above)

### Create dynamic page results by keyword

* [link to site](https://github.com/datademofun/flask-data-querystrings)

* (in conjuction with:)
* [link](https://stackoverflow.com/questions/51205939/flask-postgres-dynamic-query-string-with-variable)


#### Create dynamic download link by keyword

* [link](https://stackoverflow.com/questions/29956695/flask-dynamically-generate-link-to-file)

## Python Script 

* List all directories, subdirectories, files
* [link](https://stackoverflow.com/questions/2909975/python-list-directory-subdirectory-and-files)

* Move all files to new directory
* [link](https://stackoverflow.com/questions/27151460/how-can-i-move-files-from-one-directory-to-another)

* SMB server connection
* [link](https://stackoverflow.com/questions/20422047/copying-files-from-smb-server-to-local-drivelinux-windows-in-python/20427382)
* [documentation](http://pysmb.readthedocs.io/en/latest/api/smb_SMBConnection.html)

* terminal: SMB connection
* [link](https://superuser.com/questions/482454/how-to-cd-into-smb-user100-100-100-100-from-terminal)
* $ *smbclient -U jinny_nas //161.122.102.54/shared*

* Copy folder from smb (for script)
* [link](http://technotize.blogspot.com/2011/12/copy-folder-with-ubuntu-smb-client.html)
* [link](https://superuser.com/questions/856617/how-do-i-recursively-download-a-directory-using-smbclient)
* [link](https://www.samba.org/samba/docs/current/man-html/smbclient.1.html)

* Execute terminal command with Python script
* [link](https://stackoverflow.com/questions/3730964/python-script-execute-commands-in-terminal)
* >*import sys, os*
* >*os.system("")*

* Connect and Execute raw SQL commands with python
* [link](http://www.postgresqltutorial.com/postgresql-python/)

* Connect to Google Sheets API
* [link](https://developers.google.com/sheets/api/reference/rest/)
* [link](https://www.twilio.com/blog/2017/02/an-easy-way-to-read-and-write-to-a-google-spreadsheet-in-python.html)
* [link](https://github.com/burnash/gspread)
* [link](https://gspread.readthedocs.io/en/latest/oauth2.html)
* [credentials](https://console.developers.google.com/apis/credentials?project=injection-213609)
* [link](https://developers.google.com/sheets/api/quickstart/python)
* [link](https://developers.google.com/sheets/api/guides/libraries#python)
* [link](https://cloud.google.com/appengine/docs/standard/python3/using-cloud-sql-postgres)
* [link](https://cloud.google.com/sdk/docs/#install_the_latest_cloud_tools_version_cloudsdk_current_version)

# Backing up database

* S *sudo pg_dump -U eeum eeum > backup_file*

## Restore backup database

* [Step 4 of link](https://www.vultr.com/docs/how-to-backup-and-restore-postgresql-databases-on-ubuntu-16-04)

